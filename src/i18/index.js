import I18n from 'react-native-i18n';
import en from './en';
import vi from './vi'
I18n.fallbacks = true;
// I18n.locale = 'vi';
I18n.translations = {
    en,
    vi
};

export default i18 = I18n;