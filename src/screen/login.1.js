import React from 'react';
import { View, StyleSheet, ImageBackground, Image, Platform } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { BaseInput } from '../component/custom/BaseInput';
import { asset } from '../config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import TouchID from 'react-native-touch-id'

class Login extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            isSupportTouchID: false
        }

        const optionalConfigObject = {
            unifiedErrors: false, // use unified error messages (default false)
            passcodeFallback: false // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
        }

        TouchID.isSupported(optionalConfigObject)
            .then(biometryType => {

                console.log('====================================');
                console.log(biometryType);
                console.log('====================================');

                // Success code
                if (biometryType === 'FaceID') {
                    console.log('====================================');
                    console.log('FaceID is supported.');
                    console.log('====================================');
                } else {
                    this.setState({
                        isSupportTouchID: true
                    })
                    console.log('====================================');
                    console.log('TouchID is supported.');
                    console.log('====================================');

                }
            })
            .catch(error => {
                // Failure code
                console.log('====================================');
                console.log(error);
                console.log('====================================');
            });
    }

    onTouch = () => {
        const optionalConfigObject = {
            title: 'Authentication Required', // Android
            imageColor: '#e00606', // Android
            imageErrorColor: '#ff0000', // Android
            sensorDescription: 'Touch sensor', // Android
            sensorErrorDescription: 'Failed', // Android
            cancelText: 'Cancel', // Android
            fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
            unifiedErrors: false, // use unified error messages (default false)
            passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
        };

        TouchID.authenticate('to demo this react-native component', optionalConfigObject)
            .then(success => {
                alert('Authenticated Successfully');
            })
            .catch(error => {
                alert('Authentication Failed');
            });
    }

    render() {
        return <View source={require('../img/bg.jpg')} style={styles.container}>
            {/* <KeyboardAwareScrollView> */}
            <BaseButton style={styles.btn_term}>
                <BaseText style={styles.text_term}>{i18.t('term_of_service')}</BaseText>
            </BaseButton>
            <View style={{ paddingHorizontal: 20, flex: 1, marginBottom: 102 }}>

                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ flex: 1 }}>
                                <BaseText style={{ color: '#232d42', }} size='lg'>{i18.t('welcome')}</BaseText>
                                <BaseText style={{ color: '#ff8f3c', letterSpacing: 8 }} size='xl'>{i18.t('med')}</BaseText>
                                <BaseText style={{ color: '#c5cfde', }} size='md' >{i18.t('med_service')}</BaseText>
                            </View>
                            <Logo />
                        </View>
                        <View style={{ marginTop: 94 }}>

                            {/* <View style={{ marginTop: 20 }}> */}
                            <BaseInput placeholder={i18.t('user_name')} icon={asset.userIcon} />
                            <BaseInput placeholder={i18.t('password')} icon={asset.passwordIcon} containerStyle={{
                                marginTop: 24,
                            }} />
                            {
                                this.state.isSupportTouchID && <ImageBackground source={asset.fingerPrintIcon} style={styles.fingerPrint} >
                                    <BaseButton onPress={this.onTouch} style={{ flex: 1 }} />
                                </ImageBackground>
                            }
                        </View>
                    </View>

                </View>
            </View>
            {/* </KeyboardAwareScrollView> */}
            <ImageBackground
                source={asset.bgButtonLogin}
                style={styles.bgBtLogin}>
                <View>
                    <BaseText style={styles.underline_text} children={i18.t('dont_have_acc')} onPress={() => { this.props.navigation.push('signUp') }} />
                    <BaseText style={{ ...styles.underline_text, marginTop: 7, }} onPress={() => { this.props.navigation.push('forgotPassword') }} children={i18.t('forgot_password')} />
                </View>
                <BaseButton
                    onPress={() => {
                        this.props.navigation.navigate('home')
                    }}
                    style={styles.wrap_btn_login}>
                    <BaseText size='md' style={styles.btnLogin_text}
                        children={i18.t('signin')} />
                </BaseButton>
            </ImageBackground>
        </View >
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: 'rgb(252 ,252, 252)', paddingTop: Platform.select({
            ios: 5
        }),
    },
    underline_text: {
        color: 'white',
        textDecorationLine: 'underline'
    },
    btn_term: {
        alignSelf: 'flex-end',
        paddingVertical: 22,
        borderRadius: 15,
    },
    text_term: {
        color: 'rgb(127,181,255)',
        paddingHorizontal: 20,
        fontSize: 12,
    },
    bgBtLogin: {
        height: 102,
        backgroundColor: '#438ff6',
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    wrap_btn_login: {
        height: 46,
        width: 140,
        borderRadius: 4,
        backgroundColor: '#f9f9f9',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnLogin_text: {
        color: '#7fb5ff',
        fontWeight: '500'
    },
    fingerPrint: { marginTop: 24, resizeMode: 'contain', height: 42, width: 42, alignSelf: 'center', }
})

export default Login