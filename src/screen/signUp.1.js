import React from 'react';
import { View, StyleSheet, ImageBackground, Image, KeyboardAvoidingView } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { BaseInput } from '../component/custom/BaseInput';
import { asset } from '../config';
import Nav from '../component/custom/Nav';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
class Login extends React.Component {

    render() {
        return <View source={asset.bg} style={styles.container}>
            <Nav {...this.props} />
            <KeyboardAwareScrollView>
                <View style={{ paddingHorizontal: 20, flex: 1, marginBottom: 102 }}>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <BaseText children={i18.t('signUp')} style={{ marginTop: 82, fontWeight: '500' }} size='lg' />
                        <View style={{ marginTop: 94 }}>
                            <BaseInput placeholder={i18.t('user_name')} icon={asset.userIcon} size='xs' />
                            <BaseInput placeholder={i18.t('tel_num')} icon={asset.tel} size='xs' containerStyle={styles.inputMarginTop} />
                            <BaseInput placeholder={i18.t('password')} icon={asset.passwordIcon} size='xs' containerStyle={styles.inputMarginTop} />
                            <BaseInput placeholder={i18.t('retype')} icon={asset.passwordIcon} size='xs' containerStyle={styles.inputMarginTop} />
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>

            <ImageBackground
                source={asset.bgButtonLogin}
                style={styles.bgBtLogin}>
                <BaseButton
                    style={styles.wrap_btn_login}>
                    <BaseText size='md' style={styles.btnLogin_text}
                        children={i18.t('done')} />
                </BaseButton>
            </ImageBackground>
        </View>
    }
}

let styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'rgb(252 ,252, 252)' },
    underline_text: {
        color: 'white',
        textDecorationLine: 'underline'
    },
    btn_term: {
        alignSelf: 'flex-end',
        paddingVertical: 22,
        borderRadius: 15,
    },
    text_term: {
        color: 'rgb(127,181,255)',
        paddingHorizontal: 20,
    },
    bgBtLogin: {
        height: 102,
        backgroundColor: '#438ff6',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrap_btn_login: {
        height: 46,
        width: 140,
        borderRadius: 4,
        backgroundColor: '#f9f9f9',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnLogin_text: {
        color: '#7fb5ff',
        fontWeight: '500'
    },
    fingerPrint: { marginTop: 24, resizeMode: 'contain', height: 42, width: 42, alignSelf: 'center', },
    inputMarginTop: {
        marginTop: 24,
    }
})

export default Login