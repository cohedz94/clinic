import React from 'react';
import { View, StyleSheet, StatusBar, Keyboard, Platform } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { BaseInput } from '../component/custom/BaseInput';
import { asset, LargeDimension, screenHeight } from '../config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import Nav, { Head } from '../component/custom/Nav';

class Login extends React.Component {

    signIn = () => {
        // console.log('====================================');
        // console.log('un', this.inputUserName._lastNativeText);
        // console.log('====================================');
        // console.log('====================================');
        // console.log('pw', this.inputPassword._lastNativeText);
        // console.log('====================================');
    }

    render() {
        return <View style={styles.container}>
            <StatusBar backgroundColor={'#4b93f4'} />
            <LinearGradient colors={['#4b93f4', '#2b7bff']} style={styles.gadient}>
                <Head
                    back
                    {...this.props}
                />
                <BaseText children={i18.t('signin')} style={styles.titlePage} size='lg' />
                <KeyboardAwareScrollView
                    scrollEnabled={false}
                    enableOnAndroid={true}
                >
                    <Logo style={{ alignSelf: 'center', marginTop: 12 }} />

                    <View style={styles.wrapCenter}>
                        <BaseInput placeholder={i18.t('user_name')} icon={asset.userIcon} references={r => this.inputUserName = r}
                            returnKeyType={"next"}
                            onSubmitEditing={() => {
                                this.inputPassword.focus()
                            }}
                        />
                        <BaseInput placeholder={i18.t('password')} icon={asset.passwordIcon} references={r => this.inputPassword = r} containerStyle={{
                            marginTop: 24
                        }}
                            returnKeyType={"done"}
                            onSubmitEditing={this.signIn}
                            maxLength={15}
                            pass
                        />
                    </View>
                </KeyboardAwareScrollView>
            </LinearGradient>

            <View style={styles.wrapButton}>
                <BaseButton
                    onPress={() => { this.props.navigation.navigate('viewtest') }}
                    style={styles.button}>
                    <BaseText style={{ color: 'white', fontWeight: '500' }} size='md'>{i18.t('signin')}</BaseText>
                </BaseButton>
            </View>

            <BaseText style={styles.bottomText}
                onPress={() => { this.props.navigation.navigate('forgotPassword') }}>{i18.t('forgot_password')}</BaseText>
        </View >
    }
}

let styles = StyleSheet.create({
    container: { flex: 1, minHeight: 0.9 * screenHeight, backgroundColor: 'rgb(252,252,252)' },
    gadient: { flex: 1, borderBottomRightRadius: 20, borderBottomLeftRadius: 20 },
    titlePage: { color: 'white', fontWeight: '500', marginTop: LargeDimension ? 8 : 0, marginLeft: 20 },
    wrapInput: { paddingHorizontal: 20, justifyContent: 'center', flex: 1, marginTop: 20 },
    wrapButtonSecton: {
        flexDirection: 'row', paddingHorizontal: 26, justifyContent: 'space-between', marginTop: 24, marginBottom: 52,
    },
    wrapButton: {
        alignSelf: 'center', width: LargeDimension ? 150 : 125, height: LargeDimension ? 46 : 38,
        justifyContent: 'center', alignItems: 'center', backgroundColor: "rgb(255,143,60)",
        borderRadius: 23,
        shadowColor: "rgb(255,143,60)",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: .5,
        shadowRadius: 4.65,

        elevation: 4,
        marginTop: LargeDimension ? 24 : 18
    },
    button: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: "rgb(255,143,60)",
        borderRadius: 23, width: '100%', height: '100%'
    },
    btnText: { color: 'white', fontWeight: '500' },
    bottomText: { textDecorationLine: 'underline', color: 'rgb(156,163,177)', alignSelf: 'center', marginTop: LargeDimension ? 32 : 10, marginBottom: LargeDimension ? 50 : 25 },
    wrapCenter: { paddingHorizontal: 20, paddingTop: 30 }

})

export default Login