import React from 'react';
import { View, StyleSheet, ImageBackground, Image, Platform, Keyboard, Dimensions, Text } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { BaseInput } from '../component/custom/BaseInput';
import { asset, LargeDimension, screenHeight } from '../config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'

import LinearGradient from 'react-native-linear-gradient';
import { Head } from '../component/custom/Nav';

class VerifyOTP extends React.Component {

    state = {
        duration: 15
    }

    componentDidMount() {
        this.loadTime()
    }

    loadTime = () => {
        if (this.state.duration) {
            this.to = setTimeout(() => {
                this.setState({ duration: this.state.duration - 1 });
                this.loadTime()
            }, 1000);

        }
    }

    componentWillUnmount() {
        clearTimeout(this.to)
    }

    render() {
        return <View style={styles.container}>
            <LinearGradient colors={['#4b93f4', '#2b7bff']} style={styles.gadient}>
                <Head
                    back
                    {...this.props}
                />
                <BaseText children={i18.t('verifyOTP')} style={styles.titlePage} size='lg' />
                <KeyboardAwareScrollView
                    extraScrollHeight={10}
                    scrollEnabled={false}
                    enableOnAndroid={true}
                >
                    <Logo style={styles.wrapLogo} />
                    <View style={{ paddingHorizontal: 20, flex: 1, marginTop: LargeDimension ? 70 : 50 }}>
                        <BaseText children={`${i18.t('timeoutOTP')} ${this.state.duration}s`} style={{ color: 'white', marginBottom: 12 }} />
                        <BaseInput placeholder={i18.t('inputOTP')}
                            textLeft={'OTP'}
                            references={r => this.inputUserName = r}
                            keyboardType='number-pad'
                            maxLength={6}
                            size='md'
                        />
                    </View>
                </KeyboardAwareScrollView>
            </LinearGradient>
            <View style={styles.wrapButton}>
                <BaseButton onPress={this.signIn} style={styles.button}>
                    <BaseText style={styles.btnText} size='md'>{i18.t('verify')}</BaseText>
                </BaseButton>
            </View>
        </View>
    }
}

let styles = StyleSheet.create({
    container: { flex: 1, minHeight: 0.9 * screenHeight, backgroundColor: 'rgb(252,252,252)' },
    gadient: { flex: 1, borderBottomRightRadius: 20, borderBottomLeftRadius: 20 },
    titlePage: { color: 'white', fontWeight: '500', marginTop: LargeDimension ? 8 : 0, marginLeft: 20 },
    wrapInput: { paddingHorizontal: 20, justifyContent: 'center', flex: 1, marginTop: 20 },
    wrapButtonSecton: {
        flexDirection: 'row', paddingHorizontal: 26, justifyContent: 'space-between', marginTop: 24, marginBottom: 52,
    },
    wrapButton: {
        height: 46, width: 150, alignSelf: 'center',
        justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(255,143,60)',
        borderRadius: 23,
        shadowColor: 'rgb(255,143,60)',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: .5,
        shadowRadius: 4.65,

        elevation: 4,
        marginBottom: 52,
        marginTop: 24,
    },
    button: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(255,143,60)',
        borderRadius: 23, width: '100%', height: '100%'
    },
    btnText: { color: 'white', fontWeight: '500' },
    wrapLogo: { alignSelf: 'center', marginTop: 12 }
})

export default VerifyOTP