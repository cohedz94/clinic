import React from 'react';
import { Text, View, ImageBackground, StyleSheet, Image, FlatList, ScrollView } from "react-native";
import { asset, fontSize, LargeDimension, screenWidth } from '../../config';
import { BaseText } from '../../component/custom/BaseText';
import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../../i18'
import BaseBlock from '../../component/custom/BaseBlock';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import CalendarStrip from "react-native-calendar-strip";
import { Head } from '../../component/custom/Nav';
import LinearGradient from 'react-native-linear-gradient';
import { BaseTextInput } from '../../component/custom/BaseInput';
import SelectDoctor from './SelectDoctor';
import SelectPatient from './SelectPatient';
import ConfirmBooking from './ConfirmBooking';
import SelectTime from './SelectTime';
export default class Booking extends React.PureComponent {

  state = {
    step: 3,
    chosen: 'offline',
    chosenpatient: 113,
  }

  renderStep = () => {
    switch (this.state.step) {
      case 1:
        return <SelectPatient/>
        break;
      case 2:
        return <SelectDoctor />
        break;
      case 3:
        return <SelectTime/>
        break;
      case 4:
        return <ConfirmBooking/>
        break;
      default:

    }
  }

  jumpStep = (add = 1) => {
    this.setState({
      step: this.state.step + add
    })
  }

  renderFooter = () => {

    let { step } = this.state;

    let button;


    switch (step) {
      case 1:
        button = [
          {
            text: i18.t('chooseDoctor'),
            action: () => {
              this.jumpStep(1)
            }
          }
        ]
        break;
      case 2:
        button = [
          {
            text: i18.t('time'),
            action: () => {
              this.jumpStep(1)
            }
          },
          {
            text: i18.t('goBack'),
            action: () => {
              this.jumpStep(-1)
            }
          }

        ]
        break;
      case 3:
        button = [

          {
            text: i18.t('confirm'),
            action: () => {
              this.jumpStep(1)
            }
          },
          {
            text: i18.t('goBack'),
            action: () => {
              this.jumpStep(-1)
            }
          },
        ]
        break;
      case 4:
        button = [
          {
            text: i18.t('complete'),
            action: () => {
              this.jumpStep(-3)
            }
          },
          {
            text: i18.t('goBack'),
            action: () => {
              this.jumpStep(-1)
            }
          },
        ]
        break;
      default:
        button = []
        break;
    }

    return <View style={{ height: 94, flexDirection: 'row-reverse', width: '100%', justifyContent: 'space-between', paddingRight: 38, paddingLeft: 47 }}>
      {
        button.map((el, index) => {
          return <BaseButton onPress={el.action} key={el.text} >
            <BaseText children={el.text} style={{ color: index == 0 || button.length == 1 ? "rgb(255,143,60)" : 'rgb(35,45,66)', marginTop: 26 }} size='md' />
          </BaseButton>
        })
      }
    </View>
  }

  render() {

    let titlePage = i18.t('booking');
    switch (this.state.step) {
      case 2:
        titlePage = i18.t('chooseDoctor');
        break;
      case 4:
        titlePage = i18.t('confirmTime');
        break;
      default:
        break;
    }

    return <View style={{ flex: 1, backgroundColor: 'rgb(252,252,252)' }}>
      <LinearGradient colors={['#4b93f4', '#2b7bff']} style={{ flex: 1 }}>
        <Head
          // left={<Image source={asset.menu} style={{ height: 16, width: 16 }} />}
          back
          {...this.props}
          // onLeftPress={() => { this.props.navigation.openDrawer() }}
          right={<BaseText children={i18.t('cancel')} style={{ color: '#fff' }} />}
          onRightPress={() => { this.setState({ step: 1 }) }}
        />
        <BaseText children={titlePage} style={{ marginTop: 10, color: '#fff', fontWeight: '500', marginLeft: 20 }} size='lg' />

        {
          this.renderStep()
        }
        <View style={{ height: 5, width: 134, borderRadius: 2, alignSelf: 'center', marginTop: 12, marginBottom: 12, backgroundColor: 'rgba(255,255,255,0.3)' }}>
          <View style={{ width: `${this.state.step / 0.04}%`, height: '100%', backgroundColor: '#fff', borderRadius: this.state.step == 4 ? 2 : 0, borderBottomLeftRadius: 2, borderTopLeftRadius: 2, }} />
        </View>
      </LinearGradient>

      {
        this.renderFooter()
      }

    </View >
  }
}