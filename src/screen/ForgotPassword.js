import React from 'react';
import { View, StyleSheet, ImageBackground, Image } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { BaseInput } from '../component/custom/BaseInput';
import { asset } from '../config';
import Nav from '../component/custom/Nav';
class ForgotPassword extends React.Component {

    state = {
        screen: '',
        timeOtpExperied: 0
    }

    getOTP = () => {
        if (!this.state.timeOtpExperied) return;
        this.state.timeOtpExperied--;
        this.setState({ screen: 'otp' });
        this.to = setTimeout(() => {
            this.getOTP()
        }, 1000);
    }

    componentWillUnmount() {
        clearTimeout(this.to);
    }

    render() {

        let isOtp = this.state.screen == 'otp';
        const { timeOtpExperied } = this.state;

        return <View source={asset.bg} style={styles.container}>
            <Nav {...this.props} title={i18.t('forgot_password')}/>
            <View style={{ paddingHorizontal: 20, flex: 1, marginBottom: 102 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <BaseText style={{ color: '#232d42', flex: 1, }} size='lg' children={i18.t('for_pass')} />
                    <Logo />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <BaseText children={i18.t(isOtp ? 'time_out_otp' : 'forgot_pw_text')} style={{ alignSelf: 'center', color: '#32435b' }} size='xs' />
                    <View style={{ marginTop: 12 }}>
                        {
                            isOtp ?
                                <BaseInput placeholder={`${Math.floor(timeOtpExperied / 60)}:${(timeOtpExperied % 60) || '00'}`} icon={asset.time} /> : [
                                    <BaseInput placeholder={i18.t('user_name')} icon={asset.userIcon} key={1} />,
                                    <BaseInput placeholder={i18.t('user_name')} icon={asset.tel} containerStyle={styles.inputMarginTop} key={0} />
                                ]
                        }
                    </View>
                </View>
            </View>

            <ImageBackground
                source={asset.bgButtonLogin}
                style={styles.bgBtLogin}>
                <BaseButton
                    onPress={() => {
                        this.state.timeOtpExperied = 15;
                        this.getOTP();
                    }}
                    style={styles.wrap_btn_login}>
                    <BaseText size='md' style={styles.btnLogin_text}
                        children={i18.t(isOtp ? 'done' : 'get_password')} />
                </BaseButton>
            </ImageBackground>
        </View>
    }
}

let styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'rgb(252 ,252, 252)' },
    underline_text: {
        color: 'white',
        textDecorationLine: 'underline'
    },
    btn_term: {
        alignSelf: 'flex-end',
        paddingVertical: 22,
        borderRadius: 15,
    },
    text_term: {
        color: 'rgb(127,181,255)',
        paddingHorizontal: 20,
    },
    bgBtLogin: {
        height: 102,
        backgroundColor: '#438ff6',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrap_btn_login: {
        height: 46,
        width: 170,
        borderRadius: 4,
        backgroundColor: '#f9f9f9',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnLogin_text: {
        color: '#7fb5ff',
        fontWeight: '500'
    },
    fingerPrint: { marginTop: 24, resizeMode: 'contain', height: 42, width: 42, alignSelf: 'center', },
    inputMarginTop: {
        marginTop: 24,
    }
})

export default ForgotPassword