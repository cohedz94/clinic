import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableOpacity,
    Keyboard,
    AsyncStorage
} from "react-native";
import { StringeeClient } from "stringee-react-native";
// import FCM from "react-native-fcm";
// import { FCMEvent } from "react-native-fcm";

const iOS = Platform.OS === "ios" ? true : false;

export default class HomeCall extends Component {
    constructor(props) {
        super(props);

        this.state = {
            myUserId: "",
            callToUserId: "",
            hasConnected: false
        };

        this.clientEventHandlers = {
            onConnect: this._clientDidConnect,
            onDisConnect: this._clientDidDisConnect,
            onFailWithError: this._clientDidFailWithError,
            onRequestAccessToken: this._clientRequestAccessToken,
            onIncomingCall: this._callIncomingCall
        };
    }

    componentWillMount() { }

    token = [
        'eyJjdHkiOiJzdHJpbmdlZS1hcGk7dj0xIiwidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJqdGkiOiJTS1FmeWtpQkNCS0tuZTQ0WG5seGRjT0U5NmFnRzBMT0NnLTE1NTA4MjAyODEiLCJpc3MiOiJTS1FmeWtpQkNCS0tuZTQ0WG5seGRjT0U5NmFnRzBMT0NnIiwiZXhwIjoxNTUzNDEyMjgxLCJ1c2VySWQiOiIxMjMiLCJpY2NfYXBpIjp0cnVlfQ.cO-3PWq6n6drWsj5uwdriZi1d4ow7sl5CflHSovin_Y',
        'eyJjdHkiOiJzdHJpbmdlZS1hcGk7dj0xIiwidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJqdGkiOiJTS1FmeWtpQkNCS0tuZTQ0WG5seGRjT0U5NmFnRzBMT0NnLTE1NTA4MjAzNTQiLCJpc3MiOiJTS1FmeWtpQkNCS0tuZTQ0WG5seGRjT0U5NmFnRzBMT0NnIiwiZXhwIjoxNTUzNDEyMzU0LCJ1c2VySWQiOiI0NTYiLCJpY2NfYXBpIjp0cnVlfQ.HyZgbYm4ebbX7pUq43Bt2FS8Kzwb7P9Y2mMn0M50EdQ',
        'eyJjdHkiOiJzdHJpbmdlZS1hcGk7dj0xIiwidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJqdGkiOiJTS1FmeWtpQkNCS0tuZTQ0WG5seGRjT0U5NmFnRzBMT0NnLTE1NTA4MjA2NTAiLCJpc3MiOiJTS1FmeWtpQkNCS0tuZTQ0WG5seGRjT0U5NmFnRzBMT0NnIiwiZXhwIjoxNTUzNDEyNjUwLCJ1c2VySWQiOiI3ODkiLCJpY2NfYXBpIjp0cnVlfQ.Pi1IY4Jy5hJFSbWLGnlS4s4cE-anI5-epbRrhD60Q3U'
    ]

    async componentDidMount() {
        await this.refs.client.connect(this.token[Math.floor(Math.random() * Math.floor(3))]);
    }

    // Connection
    _clientDidConnect = ({ userId }) => {
        console.log("_clientDidConnect - " + userId);
        this.setState({
            myUserId: userId,
            hasConnected: true
        });

        // if (!iOS) {
        //   AsyncStorage.getItem("isPushTokenRegistered").then(value => {
        //     if (value !== "true") {
        //       FCM.getFCMToken().then(token => {
        //         this.refs.client.registerPush(
        //           token,
        //           true,
        //           true,
        //           (result, code, desc) => {
        //             if (result) {
        //               AsyncStorage.multiSet([
        //                 ["isPushTokenRegistered", "true"],
        //                 ["token", token]
        //               ]);
        //             }
        //           }
        //         );
        //       });
        //     }
        //   });

        //   FCM.on(FCMEvent.RefreshToken, token => {
        //     this.refs.client.registerPush(
        //       token,
        //       true,
        //       true,
        //       (result, code, desc) => { }
        //     );
        //   });
        // }
    };

    _clientDidDisConnect = () => {
        console.log("_clientDidDisConnect");
        this.setState({
            myUserId: "",
            hasConnected: false
        });
    };

    _clientDidFailWithError = () => {
        console.log("_clientDidFailWithError");
    };

    _clientRequestAccessToken = () => {
        console.log("_clientRequestAccessToken");
        // Token để kết nối tới Stringee server đã hết bạn. Bạn cần lấy token mới và gọi connect lại ở đây
        // this.refs.client.connect("NEW_TOKEN");
    };

    // Call events
    _callIncomingCall = ({
        callId,
        from,
        to,
        fromAlias,
        toAlias,
        callType,
        isVideoCall,
        customDataFromYourServer
    }) => {
        console.log(
            "IncomingCallId-" +
            callId +
            " from-" +
            from +
            " to-" +
            to +
            " fromAlias-" +
            fromAlias +
            " toAlias-" +
            toAlias +
            " isVideoCall-" +
            isVideoCall +
            "callType-" +
            callType +
            "customDataFromYourServer-" +
            customDataFromYourServer
        );

        this.props.navigation.navigate("callScreen", {
            callId: callId,
            from: from,
            to: to,
            isOutgoingCall: false,
            isVideoCall: isVideoCall
        });
    };

    // Action
    _onVoiceCallButtonPress = () => {
        console.log("_onVoiceCallButtonPress");
        Keyboard.dismiss();
        if (this.state.callToUserId != "" && this.state.hasConnected) {
            this.props.navigation.navigate("callScreen", {
                from: this.state.myUserId,
                to: this.state.callToUserId,
                isOutgoingCall: true,
                isVideoCall: false
            });
        }
    };

    _onVideoCallButtonPress = () => {
        Keyboard.dismiss();
        console.log("_onVideoCallButtonPress");
        if (this.state.callToUserId != "" && this.state.hasConnected) {
            this.props.navigation.navigate("callScreen", {
                from: this.state.myUserId,
                to: this.state.callToUserId,
                isOutgoingCall: true,
                isVideoCall: true
            });
        }
    };

    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.info}>Logged in as: {this.state.myUserId}</Text>

                <TextInput
                    underlineColorAndroid="transparent"
                    style={styles.input}
                    autoCapitalize="none"
                    value={this.state.callToUserId}
                    placeholder="Make a call to userId"
                    onChangeText={text => this.setState({ callToUserId: text })}
                />

                <View style={styles.buttonView}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this._onVoiceCallButtonPress}
                    >
                        <Text style={styles.text}>Voice Call</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={this._onVideoCallButtonPress}
                    >
                        <Text style={styles.text}>Video Call</Text>
                    </TouchableOpacity>
                </View>

                <StringeeClient ref="client" eventHandlers={this.clientEventHandlers} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
        fontWeight: "bold"
    },
    info: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
        fontWeight: "bold",
        color: "red"
    },

    text: {
        textAlign: "center",
        color: "#F5FCFF",
        marginBottom: 5,
        fontWeight: "bold",
        fontSize: 15
    },

    input: {
        height: 35,
        width: 280,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 20,
        textAlign: "center",
        backgroundColor: "#ECECEC"
    },

    button: {
        width: 120,
        height: 40,
        marginTop: 40,
        paddingTop: 10,
        // paddingBottom: ,
        backgroundColor: "#1E6738",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#fff"
    },

    buttonView: {
        width: 280,
        height: 80,
        flexDirection: "row",
        justifyContent: "space-between"
    }
});
