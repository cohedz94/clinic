import React from 'react';
import { View, StyleSheet, Image, StatusBar } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { asset, LargeDimension, screenWidth, } from '../config';

import LinearGradient from 'react-native-linear-gradient';
import UI from '../component/UI'

import { StackActions, NavigationActions } from 'react-navigation';
class WelComePage extends React.Component {

    // componentDidMount() {
    //     UI.showBookingSuccess(() => {
    //         UI.hideUI()
    //         const resetAction = StackActions.reset({
    //             index: 0,
    //             actions: [NavigationActions.navigate({ routeName: 'home' })],
    //         });
    //         setTimeout(() => {
    //             this.props.navigation.dispatch(resetAction);
    //         }, 5);
    //     })
    // }

    render() {
        return <View style={styles.container}>
            <StatusBar backgroundColor='#4b93f4' />
            <LinearGradient colors={['#4b93f4', '#438ff6']} style={styles.gadient}>
                <View>
                    <Image source={asset.welcome} style={{ width: '80%', height: screenWidth * .8, maxWidth: 320, maxHeight: 320, alignSelf: 'center', resizeMode: 'stretch' }} />
                    <BaseText style={styles.medText}>{i18.t('med')}</BaseText>
                    <BaseText style={styles.serviceText} size='md'>{i18.t('med_service')}</BaseText>
                </View>

            </LinearGradient>
            <View style={styles.wrapCenter}>
                <View style={styles.wrapButton}>
                    <BaseButton onPress={this.signIn}
                        onPress={() => {
                            this.props.navigation.navigate('booking')
                        }}
                        style={styles.button}>
                        <BaseText style={styles.btnText} size='md'>Facebook</BaseText>
                    </BaseButton>
                </View>
                <View style={{
                    ...styles.wrapButton, backgroundColor: 'rgb(255,143,60)',
                    shadowColor: 'rgb(255,143,60)',
                }}>
                    <BaseButton onPress={this.signIn}
                        onPress={() => { this.props.navigation.navigate('login') }}
                        style={{
                            ...styles.button, backgroundColor: 'rgb(255,143,60)',
                        }}>
                        <BaseText style={styles.btnText} size='md'>{i18.t('signin')}</BaseText>
                    </BaseButton>
                </View>
            </View>
            <BaseText style={styles.bottomText} onPress={() => { this.props.navigation.navigate('signUp') }}>{i18.t('dont_have_acc')}</BaseText>
        </View >
    }
}

let styles = StyleSheet.create({
    container: { height: '100%', backgroundColor: 'rgb(252,252,252)' },
    gadient: { flex: 1, paddingTop: 5, borderBottomRightRadius: 20, borderBottomLeftRadius: 20, justifyContent: 'center' },
    titlePage: { color: 'white', fontWeight: '500', marginTop: LargeDimension ? 8 : 0, marginLeft: 20 },
    wrapInput: { paddingHorizontal: 20, justifyContent: 'center', flex: 1, marginTop: 20 },
    wrapButtonSecton: {
        flexDirection: 'row', paddingHorizontal: 26, justifyContent: 'space-between', marginTop: 24, marginBottom: 52,
    },
    wrapButton: {
        alignSelf: 'center', width: LargeDimension ? 150 : 125, height: LargeDimension ? 46 : 38,
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#438ff6',
        borderRadius: 23,
        shadowColor: '#438ff6',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: .5,
        shadowRadius: 4.65,

        elevation: 4,
    },
    button: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#438ff6',
        borderRadius: 23, width: '100%', height: '100%'
    },
    btnText: { color: 'white', fontWeight: '500' },
    medText: { fontWeight: '500', fontSize: 32, color: 'white', marginTop: 16, alignSelf: 'center', letterSpacing: 9.48, fontFamily: 'anton' },
    wrapCenter: {
        flexDirection: 'row', paddingHorizontal: 26, justifyContent: 'space-between', marginTop: LargeDimension ? 24 : 18
    },
    serviceText: { color: 'white', marginTop: 1, alignSelf: 'center' },
    bottomText: { textDecorationLine: 'underline', color: 'rgb(156,163,177)', alignSelf: 'center', marginTop: LargeDimension ? 32 : 10, marginBottom: LargeDimension ? 50 : 25 }
})

export default WelComePage