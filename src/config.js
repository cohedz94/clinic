import { Dimensions, Platform } from 'react-native'
export default config = {
    domain: 'https://facebook.github.io/'
}


const { height, width } = Dimensions.get('screen');

export const LargeDimension = width > 350
export const screenHeight = height;
export const screenWidth = width;

let platformAndroid = Platform.OS === 'android'

export const fontSize = {
    xl: platformAndroid ? 24 : 22,
    lg: platformAndroid ? 20 : 18,
    md: platformAndroid ? 16 : 14,
    sm: platformAndroid ? 14 : 12,
    xs: platformAndroid ? 12 : 10
}

export const asset = {
    bg: require('./img/bg.jpg'),
    userIcon: require('./img/user.png'),
    passwordIcon: require('./img/pw.png'),
    fingerPrintIcon: require('./img/fing.png'),
    bgButtonLogin: require('./img/bgLogin.png'),
    back: require('./img/Left.png'),
    tel: require('./img/tel.png'),
    time: require('./img/time.png'),
    close: require('./img/close.png'),
    arrowDown: require('./img/arrowDown.png'),
    defautAVT: require('./img/avatar.png'),
    menu: require('./img/menu.png'),
    welcome: require('./img/welcome.png'),
    pencil: require('./img/pencil.png'),
    clock: require('./img/clock.png'),
    radioUncheck: require('./img/radioUncheck.png'),
    radioChecked: require('./img/radioChecked.png'),
    plus: require('./img/plus.png'),
    find: require('./img/find.png'),
    star: require('./img/star.png'),
    starOutline: require('./img/starOutline.png'),
    bookSuccess: require('./img/bookingSuccess.png'),
}

export const color = {
    transparent: 'transparent'
}

export const fontFamily = 'montserrat'