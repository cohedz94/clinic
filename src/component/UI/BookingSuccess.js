import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { BaseText } from '../custom/BaseText';
import i18 from '../../i18';
import action_type from '../../redux/action_type';
import { BaseButton } from 'react-native-gesture-handler';
import { asset } from '../../config';

export default class BookingSuccess extends React.PureComponent {
    render() {
        if (this.props.show !== action_type.SHOW_BOOKING_SUCCESS) {
            return null;
        }
        return <View style={styles.container}>
            <BaseText children={i18.t('completed')} style={styles.title} bold size='lg' />
            <Image source={asset.bookSuccess} style={styles.iconSuccess} />
            <BaseText children={i18.t('thanks')} style={styles.thanksText} />
            <BaseText children={i18.t('continue_support')} style={styles.support} />
            <TouchableOpacity style={styles.wrapButton} activeOpacity={1} onPress={this.props.action}>
                <BaseText children={i18.t('return_home')} style={styles.btnText} size='lg' />
            </TouchableOpacity>
        </View>
    }
}

const styles = StyleSheet.create({
    container: { paddingHorizontal: 44, paddingVertical: 16, backgroundColor: '#fff', borderRadius: 12, width: '80%', maxWidth: 335, alignItems: 'center', },
    title: { color: '#000', marginTop: 15 },
    iconSuccess: {
        height: 76, width: 77.8, marginVertical: 32
    },
    wrapButton: { paddingVertical: 15, paddingHorizontal: 15, marginTop: 23, justifyContent: 'center', alignItems: 'center' },
    btnText: { color: 'rgb(255,143,60)' },
    support: { textAlign: 'center', marginTop: 20 },
    thanksText: { textAlign: 'center' }
})

BookingSuccess.defaultProps = {
    action: () => { }
}