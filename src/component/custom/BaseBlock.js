import React from 'react';
import { View } from 'react-native'
import { LargeDimension } from '../../config';
import { BaseButton } from 'react-native-gesture-handler';



export default BaseBlock = (props) => {
    let Container = View;

    if (props.button) {
        Container = BaseButton
    }
    return <Container
        {...props}
        style={{
            backgroundColor: 'rgba(255,255,255,0.2)',
            // borderLeftWidth: 3,
            borderRadius: 12,
            // borderWidth: 1,
            // borderRightColor: '#e5eced',
            // borderBottomColor: '#e5eced',
            // borderColor: '#e5eced',
            // borderLeftColor: '#ff8f3c',
            flexDirection: 'row',
            alignItems: 'center',
            height: LargeDimension ? 55 : 45,
            ...props.style
        }}
    >
        {props.children}</Container>
}