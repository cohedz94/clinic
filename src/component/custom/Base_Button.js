import React from 'react';

import { TouchableNativeFeedback, TouchableWithoutFeedback, Platform, View } from 'react-native';

export default Base_Button = (props) => {
    let B = TouchableNativeFeedback;
    if (Platform.OS === 'ios') B = TouchableWithoutFeedback;

    return (
        <B  {...props}>
            <View style={props.style}>{props.children}</View>
        </B>
    )
}