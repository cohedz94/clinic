import React from 'react';
import { Text, Image } from 'react-native'
import { fontSize, fontFamily, LargeDimension } from '../../config';
import api from '../../api';

export const BaseText = (props) => {
    return <Text
        {...props}
        style={{
            fontSize: fontSize[props.size] || fontSize.sm,
            fontFamily: fontFamily,
            color: '#000',
            fontWeight: props.bold ? '500' : 'normal',
            ...props.style,
        }}
    >
        {
            props.children
        }
    </Text>
}


export const Logo = (props) => {
    return <Image source={require('../../img/waterLogo.png')} style={{ height: LargeDimension ? 150 : 100, width: LargeDimension ? 148.6 : 99, ...props.style, }} />
}
