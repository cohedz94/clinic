import React from 'react';
import { Text, View, Image, Platform } from 'react-native';
import { BaseButton } from 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/FontAwesome';
import { asset, LargeDimension } from '../../config';
import { BaseText } from './BaseText';
import i18 from '../../i18';

export default Nav = (props) => {
    return <Head
        onLeftPress={props.navigation.goBack}
        left={<Image source={asset.back} style={{ height: 16, width: 8 }} />}
        center={<BaseText style={{ flex: 1, textAlign: 'center', marginRight: 40, fontWeight: '500', color: 'white' }} size={'xl'}>{props.title}</BaseText>}
    />
}

export const Head = (props) => {

    let paddingVertical = LargeDimension ? 14 : 8;

    return <View style={{ flexDirection: 'row', paddingTop: Platform.OS == 'ios' ? 25 : 10, alignItems: 'center', }}>
        {
            props.back && <BaseButton
                onPress={props.navigation.goBack}
                style={{ flexDirection: 'row', paddingVertical, alignItems: 'center', paddingHorizontal: 20 }}>
                <Image source={asset.back} style={{ height: 9, width: 5 }} />
                <BaseText children={i18.t('back')} style={{ color: '#fff', marginLeft: 7, }} />
            </BaseButton>
        }
        {
            props.left && <BaseButton
                onPress={props.onLeftPress}
                style={{ flexDirection: 'row', paddingVertical, alignItems: 'center', paddingHorizontal: 20 }}>
                {props.left}
            </BaseButton>
        }
        {
            props.center || <View style={{ flex: 1 }} />
        }
        {
            props.right && <BaseButton
                onPress={props.onRightPress}
                style={{ flexDirection: 'row', paddingVertical, alignItems: 'center', paddingHorizontal: 20 }}>
                {props.right}
            </BaseButton>
        }
    </View>
}

Head.defaultProps = {
    navigation: {
        goBack: () => {

        }
    }
}