import store from '../redux/reducer';
import action_type from '../redux/action_type';
import { Dimensions } from 'react-native';
let { height, width } = Dimensions.get('screen');
export default api = {
    screenHeight: height,
    screenWidth: width,
    convertPTtoPX:(pt)=>{
        return 1*pt;
    }
}
